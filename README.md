# Text Categorization with Relation Vectors #

This repository contains the source code for running text classification experiments (topic categorization, sentiment analysis, etc.) combining word vectors (**wvs**) and relation vectors (**rvs**). 
rvs are vectors encoded between pairs of words.

We assume that a rvs model can be represented as a graph, where nodes are words (or phrases, as long as they have a corresponding vector) and edges _are also vectors_. 
Implementations for learning such relation vectors are, for example, **SeVeN** [1], **pair2vec** [2] or **relative** [3].

This repository contains an interface for enriching the original word embedding model with relation vectors, and then run classification experiments.

## Preprocessing

We will consider three main approaches for enriching a word vector with relational information:

1) Concatenating _n_ pairs of neighbouring word + relation vector. These may be selected by some metric like PPMI.
2) Concatenating rvs, wvs or their interaction (e.g., addition or multiplication).
3) Combining wvs of incident words and their corresponding rvs into a local CNN layer.

TO-DO: Describe each step


In this step, we concatenate to a word embedding its context words and their relation vectors. These context words may have some score denoting how much they influence each other (e.g., PPMI or any other statistical pairwise association metric). It is possible to pass an optional argument with such scores to the preprocessing script.

```bash
python3 src/preprocess/conc_rvs_wvs_full.py --word-vectors YOUR-WORD-VECTORS --relation-vectors YOUR-RELATION-VECTORS --output-folder OUTPUT-FOLDER --top-n TOP-N-CONTEXT-WORDS --pairs-file PAIRWISE-SCORE-FILE
```

The result of this script is a new word embedding model where each word is enriched with relevant relational knowledge.

## Text categorization

After generating the enriched embedding model, we can pass it to the classification program, which will process the relational information in such a way that each word is enriched with the interactions between its context words and their corresponding relation vectors.

There are two different scripts, one for when the experiment is run on a dataset with provided train-test splits, and one where this is not provided and k-fold cross validation is performed.

### Train-test split provided  
Run this on your terminal:

```bash
python3 src/run/main_graph_train_test_split.py --train-path TRAINING-SET-FOLDER --test-path TEST-SET-FOLDER --tokenizer-path TOKENIZER-FILE --word-vectors YOUR-WORD-VECTORS --concatenated-vectors YOUR-ENRICHED-VECTORS --dataset-name A-LABEL-FOR-THE-DATASET --concatenated-vectors-name A-LABEL-FOR-THE-ENRICHED-VECTORS --maxlen MAX-DOC-LENGTH --top-neighbours TOP-N-CONTEXT-WORDS --dimension-relations-input SIZE-OF-RELATION-VECTORS  
```

### No train-test split provided  
Run this on your terminal:

```bash
python3 src/run/main_graph_nosplit.py --data-path DATASET-FOLDER --tokenizer-path TOKENIZER-FILE --word-vectors YOUR-WORD-VECTORS --concatenated-vectors YOUR-ENRICHED-VECTORS --dataset-name A-LABEL-FOR-THE-DATASET --concatenated-vectors-name A-LABEL-FOR-THE-ENRICHED-VECTORS  --maxlen MAX-DOC-LENGTH --top-neighbours TOP-N-CONTEXT-WORDS --dimension-relations-input SIZE-OF-RELATION-VECTORS --output-path RESULTS-DIRECTORY  
```

Note that in both cases, the program needs to have access to the `top-n` context words used in the preprocessing step, as well as the size of the input relation vectors. This is because it will `reshape` the concatenated matrix so that each context word and its relation vector are treated individually.

The only difference is that the train-test split script will print the results to std output, whereas the cross-validation script will save the results in the `RESULTS-DIRECTORY` folder.

### Data format

This repo assumes the following structure:

**Train/test split provided**
```bash
-- train-folder
	-- class1
	-- class2
	-- class3
	-- ...
-- test-folder
	-- class1
	-- class2
	-- class3
	-- ...
```
**No split provided**
```bash
-- data-folder
	-- class1
	-- class2
	-- class3
	-- ...
```

where each file `classX` has one document per line.

Two sample datasets (20news[4], comes with data split; and bbc[5], no split) are provided in the data/ folder for your convenience. We leave to the user any text normalization (casing, mwes, NER, etc.).

***

# An Example on 20news #

Download `fasttext` [6] vectors trained on English Wikipedia from [here](https://drive.google.com/file/d/1PfgFP4N8_SrUrBgs9kRt0RJNLQel5FKg/view?usp=sharing), `relative` relation vectors from [here](https://drive.google.com/file/d/1DfkBA2x1vQeh6vurtLCo8pC6DBl-hLR_/view?usp=sharing), and PPMI scores for the most co-occurring `relative` pairs from [here](https://drive.google.com/file/d/1hbGTZIAtnCwMSeSOo3xSd3WZiQp5TkYH/view?usp=sharing). Save them in the project's folder, and run:

```bash
python3 src/preprocess/conc_rvs_wvs_full.py --word-vectors fasttext_wikipedia_en_300d.vec --relation-vectors vector_pair_mincoc1.bin --output-folder . --top-n 10 --pairs-file FINAL_ppmi_pairs_topk=100_alpha_smooth=0.5.tsv_filtered.txt
```

and then:


```bash
python3 src/run/main_graph_train_test_split.py --data-path data/20news/train --tokenizer-path vectorizers/20news --word-vectors fasttext_wikipedia_en_300d.vec --concatenated-vectors concatenated__wvs=fasttext_wikipedia_en_300d.vec__rvs=vector_pair_mincoc1.bin__topneighs=10.vec --dataset-name 20news --concatenated-vectors-name fasttext__fasttext_relative  --maxlen 500 --top-neighbours 10 --dimension-relations-input 300 --output-path results/  
```



### References ###

- [1] Espinosa-Anke, L., & Schockaert, S. (2018, August). SeVeN: Augmenting Word Embeddings with Unsupervised Relation Vectors. In Proceedings of the 27th International Conference on Computational Linguistics (pp. 2653-2665).
- [2] Joshi, M., Choi, E., Levy, O., Weld, D. S., & Zettlemoyer, L. (2019, June). pair2vec: Compositional Word-Pair Embeddings for Cross-Sentence Inference. In Proceedings of the 2019 Conference of the North American Chapter of the Association for Computational Linguistics: Human Language Technologies, Volume 1 (Long and Short Papers) (pp. 3597-3608).
- [3] Camacho-Collados, J., Espinosa-Anke, L., Jameel, S., & Schockaert, S. (2019, May). A Latent Variable Model for Learning Distributional Relation Vectors. In International Joint Conferences on Artificial Intelligence.
- [4] http://qwone.com/~jason/20Newsgroups/
- [5] https://www.kaggle.com/yufengdev/bbc-text-categorization
- [6] Bojanowski, P., Grave, E., Joulin, A., & Mikolov, T. (2017). Enriching word vectors with subword information. Transactions of the Association for Computational Linguistics, 5, 135-146.
