import os

class Dataset:

    def __init__(self, base_folder):

        self.base_folder = base_folder

    def load(self):
        docs=[]
        labels=[]
        labels_idx={}
        errors=0
        files=sorted(os.listdir(self.base_folder))
        print('There are ',len(files),' files/labels')
        for filename in files:
            print('Processing documents for label: ',filename)
            if not filename in labels_idx:
                labels_idx[filename] = len(labels_idx)
            with open(os.path.join(self.base_folder,filename),'r') as f:
                for line in f:
                    docs.append(line.strip())
                    labels.append(labels_idx[filename])
        print('Found ',errors,' mal-formed documents (skipped)')    
        return docs,labels,labels_idx