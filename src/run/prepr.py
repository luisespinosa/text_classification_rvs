# we need a prepr module with code for preprocessing, tokenizer, data manager, etc
# unclear where to put the embedding handler (model, maybe?), also what to do with filter_embeddings function?

# the shared bits of the main program between train_test_split and nosplit should be in a separate module, but where?
# or merge both mains into one, and add a flag for train/test split, or add a flag for datasets

import gensim
import pickle
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
import os
import numpy as np

class WVsHandler:

    def __init__(self,embeddings_path):
        self.embeddings_path=embeddings_path

    def load(self):
        print('Loading embeddings:',self.embeddings_path)
        try:
            model=gensim.models.Word2Vec.load(self.embeddings_path)
        except:
            try:
                model=gensim.models.KeyedVectors.load_word2vec_format(self.embeddings_path)
            except:
                try:
                    model=gensim.models.KeyedVectors.load_word2vec_format(self.embeddings_path,binary=True)
                except:
                    sys.exit('Couldnt load embeddings')
        vocab=model.wv.index2word
        dims=model.__getitem__(vocab[0]).shape[0]
        vocab=set(vocab)
        return model,vocab,dims

def filter_embeddings(dataset_name, docs, word_vectors):
    # helper function to add header line to the filtered embeddings file
    def line_prepender(filename, line):
        with open(filename, 'r+') as f:
            content = f.read()
            f.seek(0, 0)
            f.write(line.rstrip('\r\n') + '\n' + content)
    filtered_file=word_vectors+'_filtered_with='+dataset_name+'.txt'
    if os.path.exists(filtered_file):
      print('Found filtered embedding file: ',filtered_file)
      word_vectors = filtered_file
    else:
      word_vocab = set()
      for doc in docs:
        for token in doc.split():
          word_vocab.add(token)
      word_counter=0
      with open(filtered_file,'w') as outf:
        i=0
        for line in open(word_vectors,'r'):
          if i > 0:
            w=line.split()[0]
            vec_len=len(line.split()[1:])
            if w in word_vocab:
              outf.write(line)
              word_counter += 1
              if word_counter % 1000 == 0:
                print('[Filtering] Keeping ',word_counter,' embeddings')
          i+=1
      word_vectors = filtered_file
      line_prepender(word_vectors, str(word_counter)+' '+str(vec_len)+'\n')
      print('\nFILTERED EMBEDDINGS FILTERED AND SAVED IN:\n',filtered_file)
    return word_vectors

### HELPERS TO SAVE AND LOAD KERAS TOKENIZER ###
def save_tokenizer(tokenizer, outpath):
    with open(outpath, 'wb') as handle:
        pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
    print('Tokenizer saved to: ',outpath)

def load_tokenizer(tokenizer_path):
    print('Loading tokenizer from: ',tokenizer_path)
    with open(tokenizer_path, 'rb') as handle:
        tokenizer = pickle.load(handle)
        return tokenizer

def preprocess(maxlen=500, **kwargs):
    train_docs=kwargs['train_docs']
    train_labels=kwargs['train_labels']
    test_docs=kwargs['test_docs']
    test_labels=kwargs['test_labels']
    tokenizer_path=kwargs['tokenizer_path']
    if not os.path.exists(tokenizer_path):
        t = Tokenizer()
        print('No tokenizer found, fitting to train set')
        t.fit_on_texts(train_docs)
        save_tokenizer(t, tokenizer_path)
    else:
        #print('Using tokenizer found at ',tokenizer_path)
        t = load_tokenizer(tokenizer_path)
    # convert raw docs to padded sequences of ints
    train_sequences = t.texts_to_sequences(train_docs)
    test_sequences = t.texts_to_sequences(test_docs)
    X_train = pad_sequences(train_sequences, maxlen=maxlen)
    X_test = pad_sequences(test_sequences, maxlen=maxlen)
    # convert labels to one-hot encoded vectors
    y_train = to_categorical(np.asarray(train_labels))
    y_test = to_categorical(np.asarray(test_labels))
    print('Shape of train data tensor:', X_train.shape)
    print('Shape of train label tensor:', y_train.shape)
    print('Shape of test data tensor:', X_test.shape)
    print('Shape of test label tensor:', y_test.shape)
    return t,X_train,y_train,X_test,y_test
