# system
import gensim
import os
import sys
from argparse import ArgumentParser
# model
import numpy as np
from sklearn.metrics import classification_report,confusion_matrix
from collections import defaultdict
import tensorflow as tf
from keras.callbacks import ModelCheckpoint
from keras.models import Sequential,Model
from keras.layers import Input, TimeDistributed, RepeatVector, Flatten, Dense, Bidirectional, LSTM, Activation, MaxPooling1D, Conv1D, Conv2D, MaxPooling2D, Dropout, Embedding, ActivityRegularization, concatenate, Permute, multiply, Lambda, Reshape, Concatenate
# evaluation
from sklearn.metrics import classification_report,confusion_matrix
from sklearn.model_selection import KFold
from sklearn.metrics import f1_score as f1
from sklearn.metrics import precision_score as precision
from sklearn.metrics import recall_score as recall
from sklearn.metrics import accuracy_score
# utils
import data_manager
import prepr

def make_embedding_layer(tokenizer,vector_size,embedding_vocab,embedding_model,maxlen):
    # build word embedding layer based on dataset vocabulary
    word_index=tokenizer.word_index
    embedding_matrix = np.zeros((len(word_index)+1, vector_size))
    for word, index in word_index.items():
        if word in embedding_vocab:
            embedding_matrix[word_index[word]]=embedding_model[word]
    embedding_layer = Embedding(len(word_index) + 1,
                                vector_size,
                                weights=[embedding_matrix],
                                input_length=maxlen,
                                trainable=False)
    return embedding_layer

def build_word_model(maxlen,dimwords,embedding_layer):
    # word model - input: Tensor("input_20:0", shape=(?, 500, 300), dtype=float32)
    n_timesteps=maxlen
    wmodel = Sequential()
    wmodel.add(embedding_layer)
    #wmodel.add(Bidirectional(LSTM(300, return_sequences=True), 
    #                         input_shape=(n_timesteps, dimwords), 
    #                         merge_mode='concat'))
    #wmodel.add(TimeDistributed(Dense(300, activation='sigmoid')))
    print('=== Word Model Summary ===')
    print('--------------------------')
    print('Input: ',wmodel.input)
    print('--------------------------')
    print(wmodel.summary())
    return wmodel

#gmodel = build_graph_model(maxlen, numb_neighs, dimwords, dimrels_orig, dimrels, embedding_layer_graph)
def build_graph_model(maxlen, numb_neighs, dimwords, dimrels, dimconc, embedding_layer, numb_filters = 200):
    # graph model
    filters=numb_filters
    kernel_size=1
    cnn_activation='relu'
    final_features = 10
    gmodel = Sequential()
    gmodel.add(embedding_layer_graph)
    # In dimwords*2 we assume original relation vectors are same dimension as word vectors
    gmodel.add(Reshape((maxlen,numb_neighs,dimwords+dimrels), input_shape=(maxlen,dimconc)))
    gmodel.add(Conv2D(filters, 
                     kernel_size=1, 
                     activation=cnn_activation, 
                     name = 'conv2d',
                     input_shape=(maxlen, numb_neighs, dimwords+dimrels)
                    )
             )
    gmodel.add(MaxPooling2D(pool_size=(1, filters),name='maxpooling2d', data_format='channels_first'))
    gmodel.add(Reshape((maxlen,numb_neighs), input_shape=(maxlen,1,numb_neighs)))
    #gmodel.add(Flatten())
    print('=== Graph Model Summary ====')
    print('--------------------------')
    print('Input: ',gmodel.input)
    print('--------------------------')
    print(gmodel.summary())
    return gmodel


def build_merged_model(wmodel,gmodel,labels_idx):
    # Create final merged model
    softmax_size = len(labels_idx)
    conc = concatenate([wmodel.output, gmodel.output])
    filters=128
    kernel_size=8
    cnn = Conv1D(filters,
                 kernel_size,
                 padding='valid',
                 activation='relu',
                 strides=2)(conc)
    mp = MaxPooling1D(pool_size=4)(cnn)
    conclstm = Bidirectional(LSTM(200))(mp)
    out = Dense(softmax_size)(conclstm)
    out = Activation('softmax')(out)
    merged_model = Model([wmodel.input, gmodel.input], out)
    merged_model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    print('=== Merged Model Summary ====')
    print('--------------------------')
    print('Input: ',merged_model.input)
    print('=== Full Model Summary ===')
    print(merged_model.summary())
    return merged_model

if __name__ == '__main__':

    parser = ArgumentParser()
    # data paths
    parser.add_argument('-data','--data-path', help='Dataset path', required=True)
    parser.add_argument('-tokenizer','--tokenizer-path', help='Keras tokenizer path (will fit one if it doesnt exist)', 
        required=True)
    parser.add_argument('-o','--output-path', help='Output folder to save results', required=True)
    # embeddings paths
    parser.add_argument('-wvs','--word-vectors', help='Word embeddings', required=True)
    parser.add_argument('-cvs','--concatenated-vectors', help='Relation embeddings', required=True)
    # data and vector input names (needed for model and results files)
    parser.add_argument('-dname','--dataset-name', help='Name of txt classification dataset (can be anything)', 
        required=True)
    parser.add_argument('-cvname','--concatenated-vectors-name', help='''Name of the concatenated vectors to be evaluated 
        (can be anything)''', required=True)
    # some hyperparameters
    parser.add_argument('-mx','--maxlen', help='Maximum document length (for padding)', required=True)
    parser.add_argument('-topn','--top-neighbours', help='Top neighbours considered in the concatenation step.', 
        required=True)
    parser.add_argument('-dri','--dimension-relations-input', help='''Dimension of the original relation vector space.
        (not the concatenated). Needed to define model architectres''', required=True)

    args = parser.parse_args()

    # Name the dataset (for naming keras models and results files)
    dataset_name = args.dataset_name

    # Load word and relation vectors
    word_vectors = args.word_vectors
    concatenated_vectors = args.concatenated_vectors
    wvs = prepr.WVsHandler(word_vectors)
    modelwords,vocabwords,dimwords = wvs.load()
    rvs = prepr.WVsHandler(concatenated_vectors)
    modelrels,vocabrels,dimrels = rvs.load()

    # Load a text categorization dataset (check the data/ folder)
    data_folder = args.data_path

    ### Preprocess the dataset (with keras text preprocessing tools) ###
    # tokenizer is either fitted to train split or loaded
    tokenizer_path = args.tokenizer_path # (e.g., vectorizers/20news)

    # load, index words and one-hot encode the dataset labels
    dataset = data_manager.Dataset(data_folder)
    docs,labels,labels_idx = dataset.load()

    ### Set some hyperparameters ###
    # maximum document length
    maxlen = int(args.maxlen)
    # number of neighbours (in the relation vector model graph) for each word
    # this should be equal to the top-neighbours argument used in the concatenate step
    numb_neighs = int(args.top_neighbours)
    # from previous concatenation step
    dimrels_orig = int(args.dimension_relations_input)

    vectors_name=args.concatenated_vectors_name # e.g., fasttext__fasttext_relative

    with tf.Graph().as_default():
        with tf.Session() as sess:
            config = tf.ConfigProto(allow_soft_placement=True)
            config.gpu_options.allocator_type = 'BFC'
            config.gpu_options.per_process_gpu_memory_fraction = 0.40
            config.gpu_options.allow_growth=True
            sess.run(tf.global_variables_initializer())

            # cross-validation configuration
            numb_splits = 5
            kf = KFold(n_splits=numb_splits,random_state=10, shuffle=True)
            fold_count=1
            all_scores=defaultdict(list)
            for train_index, test_index in kf.split(docs):
                # Separate train from test documents to fit keras tokenizer only to train text data
                # Dont index the whole array, do doc by doc in the list
                # Vectorize train and test splits
                train_docs=[]
                train_labels=[]
                for idx in train_index:
                    train_docs.append(docs[idx])
                    train_labels.append(labels[idx])
                test_docs=[]
                test_labels=[]
                for idx in test_index:
                    test_docs.append(docs[idx])
                    test_labels.append(labels[idx])
                # Fit tokenizer to train set
                tokenizer,X_train,y_train,X_test,y_test=prepr.preprocess(
                    maxlen=maxlen, 
                    train_docs=train_docs, 
                    train_labels=train_labels, 
                    test_docs=test_docs, 
                    test_labels=test_labels,
                    tokenizer_path=tokenizer_path
                    )
                # create embedding layers for word and graph models
                embedding_layer_word=make_embedding_layer(tokenizer,dimwords,vocabwords,modelwords,maxlen)
                embedding_layer_graph=make_embedding_layer(tokenizer,dimrels,vocabrels,modelrels,maxlen)
                # define keras models
                wmodel = build_word_model(maxlen, dimwords, embedding_layer_word)
                gmodel = build_graph_model(maxlen, numb_neighs, dimwords, dimrels_orig, dimrels, embedding_layer_graph, numb_filters=100)
                merged_model = build_merged_model(wmodel,gmodel,labels_idx)
                # by default we save all weights in the models/ folder
                filepath = "models/fold_count="+str(fold_count)+"weights__dataset="+dataset_name+"__vectors="+vectors_name+"-improvement-{epoch:02d}-{val_acc:.2f}.hdf5"
                checkpoint = ModelCheckpoint(filepath, 
                    monitor='val_acc', 
                    verbose=0, 
                    save_best_only=True, 
                    mode='max')
                callbacks_list = [checkpoint]
                # training hyperparameters
                BATCH_SIZE = 100
                epochs = 10
                # train the model
                merged_model.fit([X_train,X_train],y_train,
                    epochs=epochs,
                    batch_size=BATCH_SIZE,
                    callbacks=callbacks_list,
                    validation_data=([X_test,X_test],y_test)
                    )
                # after training, retrieve only relevant weights to this dataset and these embeddings
                target_models = []
                for m in os.listdir('models'):
                    if dataset_name in m and vectors_name in m:
                        target_models.append(m)
                # get best set of weights
                best_model = sorted(target_models, key=lambda x: float(x.replace('.hdf5','').split('-')[-1]))[-1]
                print('Best model: ',best_model)
                # initialize a new model and load best weights
                merged_model = build_merged_model(wmodel,gmodel,labels_idx)
                merged_model.load_weights(os.path.join('models',best_model))
                # run predictions and evaluate
                predictions = [np.argmax(k) for k in merged_model.predict([X_test,X_test])]
                gold_labels = [np.argmax(i) for i in y_test]
                prec_results=precision(gold_labels,predictions, average='macro')
                rec_results=recall(gold_labels,predictions, average='macro')
                f1_results=f1(gold_labels,predictions, average='macro')
                accres=accuracy_score(gold_labels,predictions)
                print('For fold ',fold_count)
                print('precision: ',prec_results)
                print('recall:',rec_results)
                print('f1:',f1_results)
                print('acc:',accres)
                print('---')
                all_scores['precision'].append(prec_results)
                all_scores['recall'].append(rec_results)
                all_scores['f1'].append(f1_results)
                all_scores['accuracy'].append(accres)
                fold_count+=1

    print('-- training done --')
    report=[]
    for i in all_scores:
        avg_res=sum(all_scores[i])/len(all_scores[i])
        report.append(i+' = '+str(avg_res))

    print('=============')
    print('For dataset: ',dataset_name)
    print('With embeddings: ',vectors_name)
    print('===Results===')
    print('<\t>'.join(report))

    output_file=os.path.join(args.output_path,'dataset='+dataset_name+'_vectors='+vectors_name+'.txt')
    with open(output_file,'w') as outf:
        outf.write(dataset_name+'\n')
        outf.write('-------------\n')
        outf.write(vectors_name+'\n')
        outf.write('-------------\n')
        outf.write(str(report))
    print('Results file saved to: ',output_file)

    """
    with tf.Graph().as_default():
        with tf.Session() as sess:
            config = tf.ConfigProto(allow_soft_placement=True)
            config.gpu_options.allocator_type = 'BFC'
            config.gpu_options.per_process_gpu_memory_fraction = 0.40
            config.gpu_options.allow_growth=True
            sess.run(tf.global_variables_initializer())
    """