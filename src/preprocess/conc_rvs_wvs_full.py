import gensim
import pickle
import os
import sys
import numpy as np
import networkx as nx
from collections import defaultdict
from argparse import ArgumentParser

class WVsHandler:

	def __init__(self,embeddings_path):
		self.embeddings_path=embeddings_path

	def load(self):
		print('Loading embeddings:',self.embeddings_path)
		try:
			model=gensim.models.Word2Vec.load(self.embeddings_path)
		except:
			try:
				model=gensim.models.KeyedVectors.load_word2vec_format(self.embeddings_path)
			except:
				try:
					model=gensim.models.KeyedVectors.load_word2vec_format(self.embeddings_path,binary=True)
				except:
					sys.exit('Couldnt load embeddings')
		vocab=model.wv.index2word
		dims=model.__getitem__(vocab[0]).shape[0]
		vocab=set(vocab)
		return model,vocab,dims

if __name__ == '__main__':

    parser = ArgumentParser()
    parser.add_argument('-wvs','--word-vectors', help='Word embeddings', required=True)
    parser.add_argument('-rvs','--relation-vectors', help='Relation embeddings', required=True)
    parser.add_argument('-o','--output-folder', help='Folder to save output vectors', required=True)
    parser.add_argument('-topn','--top-neighbours', help='Max number of context words', required=True)

    parser.add_argument('-p','--pairs-file', help='Pairs file with association score (e.g., PPMI)', 
        required=False)

    args = parser.parse_args()

    # LOAD WORD VECTORS
    wvs=WVsHandler(args.word_vectors)
    modelwords,vocabwords,dimwords=wvs.load()

    # LOAD RELATION VECTORS
    wvs=WVsHandler(args.relation_vectors)
    modelrels,vocabrels,dimrels=wvs.load()

    rvs_name = args.relation_vectors.split('/')[-1]
    wvs_name = args.word_vectors.split('/')[-1]

    # Load pairs (if provided)
    pairs_dict = defaultdict(lambda : defaultdict(float))
    if args.pairs_file:
        with open(args.pairs_file,'r') as pf:
            for line in pf:
                cols = line.strip().split()
                cent,cont,score = cols[0],cols[1],float(cols[2])
                pairs_dict[cent][cont]=score

    # CREATING RELATION VECTOR GRAPH
    print('\nCreating relation vector graph...\n')
    rvs_word_vocab = set()
    for rv in vocabrels:
        w1,w2=rv.split('__')[0],rv.split('__')[1]
        rvs_word_vocab.add((w1,w2))
    G=nx.DiGraph()
    for center,context in rvs_word_vocab:
        if pairs_dict and context in pairs_dict[center]:
            G.add_edge(center,context,score=pairs_dict[center][context]) # WITH WEIGHTS
        else:
            G.add_edge(center,context) # NO WEIGHTS

    outf = open(os.path.join(args.output_folder,
        'concatenated__wvs='+wvs_name+'__rvs='+rvs_name+'__topneighs='+args.top_neighbours+'.vec'),'w')
    outf.write(str(len(G.nodes()))+' '+str((dimwords+dimrels)*int(args.top_neighbours))+'\n')
    #input('- step by step -')
    for nidx,node in enumerate(G.nodes()):
        out=np.array([])
        final_neighs = ['x' for i in range(int(args.top_neighbours))]
        if pairs_dict:
            # sorted by score
            source_neighs = [a for a,b in sorted([(n,G[node][n]['score']) for n in G[node]],key=lambda x:x[1],reverse=True)]
        else:
             # unsorted
            source_neighs = list(G[node])
        source_neighs = source_neighs[:int(args.top_neighbours)]
        for idx,fn in enumerate(source_neighs):
            final_neighs[idx]=fn
        for n in final_neighs:
            if n in vocabwords:
                nvec = modelwords[n]
                out=np.append(out,nvec)
            else:
                out=np.append(out,np.zeros(dimwords))
            rv = node+'__'+n
            if rv in vocabrels:
                out=np.append(out,modelrels[rv])
            else:
                out=np.append(out,np.zeros(dimrels))
        outf.write(node+' '+' '.join([str(k) for k in out])+'\n')
        if nidx % 1000 == 0:
            print('Vectorized ',nidx,' of ',len(G.nodes()),' words')
    outf.close()